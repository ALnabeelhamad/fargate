
# This is the location that we want to store the state file
terraform {
  backend "s3" {
    bucket = "nabeel-dnj"
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}

# This is the location of the state file that we want to use
data "terraform_remote_state" "simplevm" {
  backend = "s3"
  config {
    bucket = "nabeel-dnj"
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}
