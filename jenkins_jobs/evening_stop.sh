#!/bin/bash

# Gets service name
aws ecs describe-services \
--cluster nabeel-production-ecs-cluster\


aws ecs update-service \
--desired-count "0" \
--service nabeel-production-web
